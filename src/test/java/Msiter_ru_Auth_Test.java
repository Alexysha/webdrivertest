import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class Msiter_ru_Auth_Test {
    private static WebDriver mDriver;

    private String login, password;

    public Msiter_ru_Auth_Test(String login, String password) {
        this.login = login;
        this.password = password;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {"#", "123"},
                {"-999999999999999999999", "123"},
                {"abc", "-------"},
                {"@mail.ru", "0-0432"}
        });
    }

    @BeforeClass
    public static void setUpClass() {
        ChromeDriverService cds = new ChromeDriverService.Builder()
                .usingAnyFreePort()
                .usingDriverExecutable(new File("C:/DriverChrome/chromedriver.exe"))
                .build();
        mDriver = new ChromeDriver(cds);
    }

    @Test
    public void testLoginAndPassword_IncorrectValue_notifyToUser() {
        mDriver.get("https://msiter.ru/user");

        Msiter_ru_LoginPage mlp = new Msiter_ru_LoginPage(mDriver);

        mlp.inputLogin().sendKeys(login);
        mlp.inputPassword().sendKeys(password);
        mlp.buttonRequestInputUserProfile().click();


        Assert.assertEquals("восстановление пароля",
                mlp.errorLoginAndPasswordNotify().getText());
    }

    @AfterClass
    public static void tearDownClass() {
        mDriver.quit();
    }
}
