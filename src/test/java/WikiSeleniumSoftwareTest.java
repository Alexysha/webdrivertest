import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;

import java.io.File;

public class WikiSeleniumSoftwareTest {
    private static WebDriver mDriver;

    @BeforeClass
    public static void setUpClass() {
        ChromeDriverService cds = new ChromeDriverService
                .Builder()
                .usingAnyFreePort()
                .usingDriverExecutable(new File("C:/DriverChrome/chromedriver.exe"))
                .build();
        mDriver = new ChromeDriver(cds);
        mDriver.get("https://www.google.com");
    }

    @Test
    public void testOpenLink_ClickLinkThoughtWorks_openPageWikiThoughtWorks() {
        GoogleHomePage ghp = new GoogleHomePage(mDriver);
        GooglePage_seleniumWiki gpsw = new GooglePage_seleniumWiki(mDriver);
        WikiPage_seleniumSoftware wpss = new WikiPage_seleniumSoftware(mDriver);

        ghp.lineSearch().sendKeys("SeleniumWiki");
        ghp.buttonSearch().click();
        gpsw.linkSeleniumSoftwareWiki().click();
        wpss.historyLink().click();
        wpss.thoughtWorksLink().click();

        Assert.assertEquals("ThoughtWorks - Wikipedia", mDriver.getTitle());
    }

    @AfterClass
    public static void tearDownClass() {
        mDriver.quit();
    }
}
