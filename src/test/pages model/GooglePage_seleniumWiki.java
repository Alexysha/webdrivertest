import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class GooglePage_seleniumWiki {
    private WebDriver mDriver;

    public GooglePage_seleniumWiki(WebDriver driver) {
        mDriver = driver;
    }

    public WebElement linkSeleniumSoftwareWiki() {
        return mDriver.findElement(By.xpath("//a[text()='Selenium (software) - Wikipedia']"));
    }
}
