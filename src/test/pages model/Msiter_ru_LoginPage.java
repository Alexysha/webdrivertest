import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Msiter_ru_LoginPage {
    private WebDriver mDriver;

    public Msiter_ru_LoginPage(WebDriver driver) {
        mDriver = driver;
    }

    public WebElement inputLogin() {
        return mDriver.findElement(By.xpath("//input[@id='edit-name']"));
    }

    public WebElement inputPassword() {
        return mDriver.findElement(By.xpath("//input[@id='edit-pass']"));
    }

    public WebElement buttonRequestInputUserProfile() {
        return mDriver.findElement(By.xpath("//input[@value='Войти']"));
    }

    public WebElement errorLoginAndPasswordNotify() {
        return mDriver
                .findElement(By.xpath("//a[text()='восстановление пароля']"));
    }
}
