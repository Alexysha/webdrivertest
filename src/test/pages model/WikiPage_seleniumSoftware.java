import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class WikiPage_seleniumSoftware {
    private WebDriver driver;

    public WikiPage_seleniumSoftware(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement historyLink() {
        return driver.findElement(By.xpath("//span[text()='History']"));
    }

    public WebElement thoughtWorksLink() {
        return driver.findElement(By.xpath("//a[text()='ThoughtWorks']"));
    }
}
