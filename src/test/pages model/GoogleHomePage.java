import org.openqa.selenium.*;

public class GoogleHomePage {
    private WebDriver driver;

    public GoogleHomePage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement lineSearch() {
        return driver.findElement(By.xpath("//input[@id='lst-ib']"));
    }

    public WebElement buttonSearch() {
        return driver.findElement(By.xpath("//input[@value='Поиск в Google']"));
    }

    public WebElement googleLogo() {
        return driver.findElement(By.xpath("//img[@alt='Google']"));
    }
}